package com.ceedcv.pacoaldariasprgexaorg.pregunta3;

import java.util.ArrayList;

/**
 *
 * @author Alba Marrades <marrades.alba@gmail.com>
 */
public class Pregunta3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Patinete> matriculas = new ArrayList<>();
        String fichero = "matriculas.txt";
        LeerFichero f = new LeerFichero(matriculas);
        f.cargarFichero(fichero);
        f.imprimirMatriculas();
    }
    
}
