package com.ceedcv.pacoaldariasprgexaorg.pregunta3;


public interface Vehiculo {
    
    public boolean arranque();
    public boolean parada();
    
}
