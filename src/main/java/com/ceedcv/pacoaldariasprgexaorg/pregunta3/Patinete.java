package com.ceedcv.pacoaldariasprgexaorg.pregunta3;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Patinete implements Vehiculo {

    private boolean arrancado = false;
    private String matricula;

    public Patinete(String matricula) {
        this.matricula = matricula;
    }

    public boolean arranque()   {
        if (!arrancado) {
            this.setArrancado(true);
        } else {
            try {
                throw new MiException("Error patinete arrancado");
            } catch (MiException ex) {
                Logger.getLogger(Patinete.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.isArrancado();
    }

    public boolean parada()  {
        if (arrancado) {
            this.setArrancado(false);

        } else {
            try {
                throw new MiException("Error patinete parado");
            } catch (MiException ex) {
                Logger.getLogger(Patinete.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.isArrancado();
    }

    /**
     * @return the arrancado
     */
    public boolean isArrancado() {
        return arrancado;
    }

    /**
     * @param arrancado the arrancado to set
     */
    public void setArrancado(boolean arrancado) {
        this.arrancado = arrancado;
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
