package com.ceedcv.pacoaldariasprgexaorg.pregunta3;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Alba Marrades <marrades.alba@gmail.com>
 * 20-may-2021 9:39:20
 */
public class LeerFichero {
    private ArrayList<Patinete> matriculas = new ArrayList<>();
    
    public LeerFichero(ArrayList<Patinete> matriculas) {
        this.matriculas = matriculas;
    }
    
    public void cargarFichero(String fichero) {
        File archivo = new File(fichero);
        String cadena;
        try {
            Scanner lector = new Scanner(archivo);
            
            while (lector.hasNext()) {
                cadena = lector.nextLine();
                String[] lista = cadena.split(",");
                for (int i = 0; i < lista.length; i++) {
                    String matricula = lista[i];
                    Patinete patinete = new Patinete(matricula);
                    //this.getMatriculas().add(patinete);
                    matriculas.add(patinete);
                }
            }
            lector.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return getMatriculas();
    }
    
    public void imprimirMatriculas() {
        for (int i = 0; i < matriculas.size(); i++) {
            Patinete get = matriculas.get(i);
            System.out.print(get.getMatricula() + ", ");
        }
    }

    /**
     * @return the matriculas
     */
    public ArrayList<Patinete> getMatriculas() {
        return matriculas;
    }

    /**
     * @param matriculas the matriculas to set
     */
    public void setMatriculas(ArrayList<Patinete> matriculas) {
        this.matriculas = matriculas;
    }
}
